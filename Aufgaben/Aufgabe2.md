# Aufgabe 2: Währungsrechner

Erstellen Sie einen einfachen Währungsrechner als Javalin-Anwendung.

![Beispielhaftes Aussehen](Aufgabe2.Bild.png)

**Lernziele**

* Sie wenden Ihre ersten HTML/CSS-Kenntnisse an
* Mit der Betragseingabe bzw. der Währungsauswahl können Sie eine HTTP-Anfrage auslösen
* Sie erstellen eine einfache Javalin-Anwendung
* Sie können in Javalin ein aktualisiertes HTML mit der HTTP-Antwort senden
* Sie können ein HTML-Dokument clientseitig mit einem HTML-Fragment aus der HTTP-Antwort aktualisieren

## Zur Aufgabe

Oben sehen Sie die grobe Idee eines Währungsrechners skizziert, der aus einem Eingabefeld für die Betragseingabe in Euro und einem Auswahlfeld für die Zielwährung besteht. Mit der Betragseingabe bzw. der Währungswahl wird sofort der in dem Eingabebereich umgerechnete Betrag angezeigt.

* Gestalten Sie den Währungsrechner mit einer Überschrift
* HTML, CSS und eventuell notwendiger JavaScript-Code fassen Sie in der Datei `index.html` zusammen
* Hübschen Sie das Aussehen mit CSS auf
* Die Währungsumrechnung geschieht ausschließlich in der Javalin-Anwendung (`App.java`)
* Aktualisieren Sie clientseitig den umgerechneten Betrag in der Datei `index.html`
* Die Wechselkurse hinterlegen Sie in der Anwendung fix in Form einer `Map<String,Float>`

Wenn Sie den Anspruchslevel etwas anheben wollen, hier ein paar Vorschläge

* Hinterlegen Sie die Wechselkurse in einem `enum`!
* Lassen Sie keine ungültigen Eingaben zu wie z.B. negative Euro-Eingaben oder ungültige Wechselkurse
* Sichern Sie die Javalin-Anwendung gegen ab gegen ungültige HTTP-Anfragen bzw. Parameterwerte
* Stellen Sie nur die in der Anwendung hinterlegten Wechselkurse zur Auswahl
* Rufen Sie die aktuellen Wechselkurse im Internet ab

## Abgabe

Liefern Sie im gepackten Format (`zip`-Datei) die mit Gradle aufgesetzte Verzeichnisstruktur ab, die jedoch bereinigt ist und nur die notwendigen Dateien enthält, namentlich

* `index.html`
* `App.java`
* `build.gradle`
* `README.md` (kein Muss, dürfen Sie aber ergänzen)

> Beachten Sie die Abgabefristen in Moodle!
