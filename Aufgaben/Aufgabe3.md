# Aufgabe 3: Zahleneingabe

Realisieren Sie die Eingabe einer Fließkommazahl über das Tastenfeld eines Taschenrechners.

**Lernziele:**

* Sie können die Funktionalität einer Anwendungslogik analysieren
* Sie können die Anwendungslogik unabhängig von einer Oberfläche umsetzen
* Sie sind in der Lage, die Funktionstauglichkeit der Anwendungslogik mit Testfällen nachzuweisen
* Sie haben die Fähigkeit, eine Oberfläche nach einfachen Kriterien der Nutzbarkeit zu gestalten  
* Sie können die Oberfläche mit der Funktionslogik verknüpfen

## Zur Aufgabe

Wenn Sie bei einem realen Taschenrechner eine Zahl eingeben, so geschieht das Zeichen für Zeichen über das Tastenfeld. Wir gehen bei dieser Aufgabe von folgendem Verhalten bei der Eingabe einer Zahl aus:

* Es ist die Eingabe von Kommazahlen möglich; die Eingabe einer Zahl in [wissenschaftlicher Notation](https://de.wikipedia.org/wiki/Wissenschaftliche_Notation) (z.B. `1,234E4`) ist nicht zulässig
* Die soweit eingegebene Zahl wird im Display des Taschenrechners angezeigt; es erfolgt _während_ der Eingabe keine Umwandlung in die wissenschaftliche Notation, so wird z.B. die Zahl `123456` nicht als `1,23456E+5` dargestellt, ebenso werden Tausenderstellen nicht durch Punkte abgesetzt, z.B. wird `1024` nicht als `1.024` dargestellt.
* Die Anzahl der Zeichen zur Eingabe und Darstellung ist begrenzt; sie ist vom Grundsatz von der Anwendungsentwickler:in frei definierbar
* Die Eingabe einer Zahl erfolgt vorzeichenlos
* Es gibt eine "Entfernen"-Taste (eine Art _undo_), die das letzte Zeichen rechts in der Zahlendarstellung entfernt; wird das letzte Zeichen entfernt, bleibt eine `0` übrig (man kann das Display in dem Sinne also nicht "leer" machen)
* Es gibt eine Löschtaste (ein _clear_), 

### Die Eingabelogik

Die zeichenweise Eingabe einer Kommazahl folgt dem beiliegenden Interface [`FloatInput.java`](Aufgabe3/FloatInput.java):

~~~ java
interface FloatInput {
    boolean put(char c); // input char by char, true if char is accepted
    float getFloat();    // get float value of input for computations 
    void undo();         // undo last input if there are chars left
    String toString();   // get string representation of input
}
~~~

Realisieren Sie die Klasse `FloatNumber` in der Datei `FloatNumber.java`, die das Interface `FloatInput` implementiert.

Die Klasse darf neben den Interface-Methoden keine weiteren Methoden haben. Die Klasse muss erfolgreich alle mitgelieferten Testfälle bestehen. Bitte legen Sie die beiliegende Testdatei [`FloatNumberTest.java`](Aufgabe3/FloatNumberTest.java) im gleichen Ordner ab wie Ihre Realisierung `FloatNumber.java` und das Interface `FloatInput.java`, so dass die Tests mit der JShell ausführbar sind:

    jshell -R-ea FloatNumberTest.java

### Die graphische Oberfläche

Setzen Sie eine graphische Oberfläche für einen simplen Taschenrechner um, die neben einem Ziffernfeld, einer Komma-Taste, Gleich-Taste, den Grundrechenarten, einer Entfernen-Taste und einer Löschtaste ein einzeiliges Zahlendisplay hat.

> Übernehmen Sie nicht einfach Ihre Lösung aus Aufgabe 1, sondern überarbeiten Sie Nutzbarkeit, Layout und Design des Erscheinungsbilds.

Die HTML-Oberfläche samt CSS- und JavaScript-Anteilen wird in der Datei `index.html` abgelegt. Der JavaScript-Anteil ist auf das Notwendigste zu beschränken, was das Erzeugen von HTTP-Requests und die Verarbeitung von HTTP-Responses angeht.

### Die Anwendung

Die Javalin-Anwendung (`App.java`) zeigt sich als simpler Taschenrechner mit den beschriebenen Eigenschaften, sie ist in der Funktionalität jedoch beschränkt auf die Eingabe von Kommazahlen.

* Nutzen Sie die Klasse `FloatNumber` für die Eingabelogik
* Ausschließlich das Zahlendisplay wird nach jeder gültigen Eingabe in der HTML-Datei aktualisiert, nicht das gesamte HTML
* Folgende Tasten reagieren auf Eingaben: Zahlenfeld, Komma-Taste, Entfernen-Taste, Lösch-Taste

Verzichten Sie bitte darauf, den Taschenrechner im Rahmen dieser Übungsaufgabe vollständig funktional zu machen. Man kann selbst bei einem simplen Taschenrechner erstaunlich viel falsch machen, von der Interaktionsgestaltung bis hin zur funktionalen Umsetzung. Wenn Sie den Anspruchslevel etwas anheben wollen, hier ein paar Vorschläge:

* Eine Fehleingabe (wenn die `put`-Methode ein `false` liefert) führt z.B. zu einem kurzen farblichen Aufblinken des Anzeigedisplays, ergänzend erfolgt ein kurzer Signalton
* Machen Sie das Erscheinungsbild _responsive_ für verschiedene Anzeigeformate und -größen; das ist anspruchsvoll, wenn es gut gemacht sein will

## Abgabe

Liefern Sie im gepackten Format (`zip`-Datei) die mit Gradle aufgesetzte Verzeichnisstruktur ab, die jedoch bereinigt ist und nur die notwendigen Dateien enthält, namentlich

* `index.html` (die HTML-Oberfläche samt CSS und minimal notwendigem JavaScript)
* `App.java` (die Javalin-Anwendung)
* `FloatInput.java` (das Interface)
* `FloatNumber.java` (die Implementierung der Eingabelogik)
* `FloatNumberTest.java` (die Tests zur Eingabelogik)
* `build.gradle` (die Build-Datei)
* `README.md` (kein Muss, dürfen Sie aber ergänzen)

> Beachten Sie die Abgabefristen in Moodle!
