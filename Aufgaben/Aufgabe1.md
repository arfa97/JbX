# Aufgabe 1: Taschenrechner-UI

Entwerfen Sie ein einfaches User Interface (Bedienungsschnittstelle) für einen Taschenrechner in HTML.

**Lernziele**

* Sie kennen den Aufbau eines HTML-Dokuments
* Sie können einfache HTML-Formatierungen per CSS vornehmen 
* Sie erstellen ein HTML-Dokument mit CSS
* Sie nutzen ein CSS-Framework, um eine einfache Oberfläche zu gestalten
* Sie sind orientiert, welche Webseiten Ihnen zu HTML und CSS eine gute Hilfe sind

## Teil A: HTML-Seite mit CSS

Erstellen Sie mit dem Konstrukt einer HTML-Tabelle eine Oberfläche für einen einfachen Taschenrechner, der neben einem Ziffernfeld und Tasten für die Grundrechenarten samt Ergebnistaste (`=`) und Vorzeichentaste (`+/-`) eine "einzeiliges" Anzeigedisplay hat.

Bemühen Sie sich um einfache CSS-Mittel, um ein wenig wegzukommen von der rudimentären Darstellungsqualität von HTML. Wenn man eine Taste mit der Maus "berührt", dann soll sie z.B. ihre Farbe wechseln.

Sie dürfen, wenn Sie Erfahrung mit HTML/CSS mitbringen, diese Aufgabe gerne mit anderen Ausdrucksmitteln als einer Tabelle lösen. 

Erstellen Sie eine einzige Datei namens `TaschenrechnerA.html`, in der auch der CSS-Code eingebettet ist. Die Datei ist ein gültiges (valides) HTML-Dokument.

## Teil B: HTML-Seite mit Bootstrap (oder Alternative)

Es gibt Bibliotheken, mit der sich sehr leicht anspruchsvolle und modern aussehende Gestaltungsmittel für die Erstellung einer HTML-basierten Oberfläche nutzen lassen. Viele dieser Bibliotheken helfen bei der Umsetzung eines _responsive Design_, d.h. dass sich die Darstellung anpasst an die Größenverhältnisse des Anzeigegeräts.

Binden Sie eine solche Bibliothek in dem HTML-Dokument ein, um die Oberfläche zum Taschenrechner ein zweites Mal, nun deutlich "schicker", umzusetzen.

Folgende Bibliotheken sind Vorschläge -- am populärsten davon ist Bootstrap. Sollten Sie eine andere Bibliothek nutzen wollen, so tun Sie das gerne.

* [Bootstrap](https://getbootstrap.com/)
* [Skeleton](http://getskeleton.com/)
* [mini.css](https://minicss.org/)

Erstellen Sie eine einzige Datei namens `TaschenrechnerB.html`, in der die Bibliothek Ihrer Wahl mit einem `<link>`-Tag eingebunden ist.




